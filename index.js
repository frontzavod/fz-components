
import form from './utils/forms';
import initMap from './utils/initmap';
import map from './utils/map';
import script from './utils/script';


const components = {
	Form: form,
	Map: map,
	initMap: initMap,
	script: script
};


export default components;