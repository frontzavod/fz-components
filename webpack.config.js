const webpack = require('webpack');
const path = require('path');

const webpackConfig = {
	devServer: {
		contentBase: path.join(__dirname, "test"),
		compress: true,
		port: 9000
	},
	entry: './test/test.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, './test')
	},
	devtool: 'cheap-module-inline-source-map',
	module: {
		loaders: [{
			test:    /\.js$/,
			loader:  'babel-loader?presets[]=env'
		}]
	},
	plugins: [
	new webpack.ProvidePlugin({
		$: 'jquery',
		jQuery: 'jquery'
	})
	]
};

module.exports = webpackConfig