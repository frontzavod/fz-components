'use strict'

let script = function(url) {
	if (Array.isArray(url)) {
		return Promise.all(url.map(item => this.script(item)));
	}

	return new Promise(function (resolve, reject) {
		let r = false;
		let t = document.getElementsByTagName('script')[0];
		let s = document.createElement('script');

		s.type = 'text/javascript';
		s.src = url;
		s.async = true;
		s.onload = s.onreadystatechange = function () {
			if (!r && (!this.readyState || this.readyState === 'complete')) {
				r = true;
				resolve(this);
			}
		};
		s.onerror = s.onabort = reject;
		t.parentNode.insertBefore(s, t);
	});
}

export default script;