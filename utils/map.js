'use strict'

let Map = function(center, zoom = 13, mapId = 'map') {
	let map = new ymaps.Map(mapId,{center: center, zoom})
	map.geoObjects.add(new ymaps.Placemark(center));
};

export default Map;


