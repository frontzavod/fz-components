'use strict'

import                     '../lib/maskedinput';
import validationfrom from 'jquery-validation';

let Form = function (form, actionFile, success) {
	this.$form = $(form);
	let $form = this.$form;
	this.mask();
	$form.on('submit', function(e){
		e.preventDefault();
		let formData = new FormData($form[0]);
		$.ajax({
			url: actionFile,
			data: formData,
			processData: false,
			contentType: false,
			type: 'POST',
			dataType: 'JSON',
			success: success
		});
	})
};

Form.prototype.mask = function() {
	this.$form.find('[type="tel"]').mask('+7(999) 999-9999');
}

export default Form;